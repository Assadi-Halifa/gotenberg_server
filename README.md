# Configuration du serveur Gotenberg

### Prérequis :
- Docker installé 
    - Windows: [Docker Desktop](https://docs.docker.com/desktop/install/windows-install/)
    - linux:  [Centos](https://docs.docker.com/engine/install/centos/), [Debian](https://docs.docker.com/engine/install/debian/),[Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
 - Docker compose soit installé faite ``` docker-compose version ``` dans le terminal pour vérifier. 
 - Definir un port q'on shoute diriger notre container par defaut 1335 donc verifier qu'il soit ouvert. sinon ouvrir un autre.
 **Remarque:** si un port autre que 1335 a été ouvert il sera necessaire de l'indiquer dans le fichier  ***docker-compose.yml***.

### configuration sans traefik:
 Placez-vous à la racine du projet la ou se trouve le fichier ***docker-compose.yml*** puis executer la commande suivant 
 ```sh
  docker compose up -d 
  ```
  le container va télécharger les dépendances puis lancer le serveur.
  Le conteneur démarré l'api et prête à être utilisé 
  si tout est bien installé rendez vous sur postman ou insomnia

et entrez l'url au format suivante :
 votre-domaine:votre-port ***/forms/libreoffice/convert*** 
 Exemple: http://localhost:1335/forms/libreoffice/convert

 **METHOD**: **POST**

 **HEADER**: Content-type: multipart/form-data

 **BODY**: file: selectnnionner un fichier format .docx

 **Response**: le fichier au format PDF.

 voir la doc de Gotenberg pour voir les autres possibilité [liens de la documentation ](https://gotenberg.dev/docs/modules/libreoffice)
  
  **Remarque:** si un port autre que 1335 a été ouvert il sera nécessaire de l'indiquer dans le fichier ***docker-compose.yml***  
  ```yml
      ports:
        - 1335:3000
  ```
modifier le 1335 qui correspond au port de la machine hôte.



 ## Source

  - Explication de Traefik + exmeple docker-compose [lien](https://traefik.me/)

  - Documentation Traefik [Doc](https://doc.traefik.io/traefik/)

  - https://blog.ouidou.fr/%C3%A0-la-d%C3%A9couverte-de-traefik-18da29cdbb46

  - https://blog.silarhi.fr/docker-compose-traefik-https/

  


 





 


